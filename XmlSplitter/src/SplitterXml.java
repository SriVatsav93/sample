import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.ObjectInputStream.GetField;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class SplitterXml {

	public static void main(String[] args) throws XMLStreamException, IOException {
		// TODO Auto-generated method stub
		XMLEventReader reader = null;
		
		String s="C:\\Users\\saiva\\Desktop\\sample.xml";
		int count=0;
		
		XMLInputFactory factory=XMLInputFactory.newInstance();
		
		XMLEventReader eventReader =
			    factory.createXMLEventReader(new FileReader(s));
		QName name=new QName("","product");
		while(eventReader.hasNext()) {
			XMLEvent event=eventReader.nextEvent();
			if(event.getEventType() == XMLStreamConstants.START_ELEMENT){
		        StartElement startElement = event.asStartElement();
		        //System.out.println(startElement.getName().getLocalPart());
		        if(startElement.getName().equals(name)) {
		        	
		        	writeToFile(eventReader,event,"C:\\Users\\saiva\\Desktop\\Split\\"+(count++)+".xml");
		        	System.out.println(count);
		        	
		        }
		     }
			if(event.isEndDocument()) {
				break;
			}
			if(event.isEndDocument()) {
				System.out.println("*******************---********");
			}
			
		}
	}
	
	public static void writeToFile(XMLEventReader reader, XMLEvent event, String fileName) throws XMLStreamException, IOException {
		int stack=1;
    	
    	StartElement startElement=event.asStartElement();
    	QName qname=startElement.getName();
    	XMLOutputFactory outputFactory=XMLOutputFactory.newInstance();
    	XMLEventWriter eventWriter=outputFactory.createXMLEventWriter(new FileWriter(fileName));
    	
    	eventWriter.add(startElement);
    	 while(true) {
    		 XMLEvent xmlEvent=reader.nextEvent();
    		 if(xmlEvent.getEventType() == XMLStreamConstants.START_ELEMENT && xmlEvent.asStartElement().getName().equals(qname)) {
    			 stack++;
    		 }
    		 	if(xmlEvent.isEndElement()) {
    		 		EndElement endElement=xmlEvent.asEndElement();
    		 		if(endElement.getName().equals(qname)) {
    		 			stack--;
    		 			if(stack==0) {
    		 				eventWriter.add(xmlEvent);
    		 				break;
    		 			}
    		 		}
    		 		
    		 	}
    		 	eventWriter.add(xmlEvent);
    		 	
    	 }
    	 eventWriter.close();
	}

}
